import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'incustTest',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
